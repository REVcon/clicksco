<?php

use TestJob\Spider;
use TestJob\Parser;
use TestJob\ProductMapper;

require 'vendor/autoload.php';
require_once "bootstrap.php";

const URL = "https://kupi.tut.by/mobilnye-telefony/?iPageNo=%d";
const REVIEW_API_URL = "https://kupi.tut.by/export/comments.php?thread_gr=cm&thread_id=%s&mode=&sort=date&how=desc&page=%d&from=11";

date_default_timezone_set("Europe/Moscow");
echo "started at " . date("H:i:s") . PHP_EOL;

$spider = new Spider();
$pageNumber = 1;
do{
    $content = $spider->query(sprintf(URL,$pageNumber));
    $parser = new Parser();
    $products = $parser->parseProducts($content);
    foreach ($products as $product) {
        $imgPage = $spider->query($product->imgLink);
        $product->imgLink = $parser->getImageLink($imgPage);
        $repeatCycle = true;
        $reviewPageNumber = 1;
        while ($repeatCycle) {
            $reviewsPage = $spider->query(sprintf(REVIEW_API_URL, $product->id, $reviewPageNumber));
            $reviews = $parser->parseReviews($reviewsPage);
            if (count($reviews) == 0) {
                $repeatCycle = false;
            }else{
                $reviewPageNumber++;
                $product->reviews = array_merge($product->reviews, $reviews);
            }
        }
        $product->reviewCount = count($product->reviews);
        $entityManager->persist(ProductMapper::Map($product));
        $entityManager->flush();
    }
    $pageNumber++;
}while (count($products) > 0);


echo "finished at ". date("H:i:s") . PHP_EOL;

