<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 29.08.2018
 * Time: 19:33
 */

namespace TestJob\Dto;


class Product
{
    public $id;
    public $name;
    public $brand;
    public $imgLink;
    public $offerCount;
    public $lowPrice;
    public $priceCurrencyCode;
    public $reviewCount;
    public $reviews;

    function __construct() {
        $this->reviews = [];
    }
}