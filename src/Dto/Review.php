<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 30.08.2018
 * Time: 15:46
 */

namespace TestJob\Dto;


class Review
{
    public $title;
    public $strength;
    public $weakness;
    public $total;
}