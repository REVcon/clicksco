<?php

namespace TestJob\Entity;

/**
 * @Entity @Table(name="review", options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"})
 **/
class Review
{
    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     **/
    protected $id;

    /**
     * @Column(type="text")
     * @var string
     **/
    protected $title;

    /**
     * @Column(type="text", nullable=true)
     * @var string
     **/
    protected $strength;


    /**
     * @Column(type="text", nullable=true)
     * @var string
     **/
    protected $weakness;

    /**
     * @Column(type="text", nullable=true)
     * @var string
     **/
    protected $total;

    /**
     * @ManyToOne(targetEntity="Product", inversedBy="reviews", cascade={"persist"})
     * @JoinColumn(name="product_id", referencedColumnName="id")
     **/
    protected $product;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param string $strength
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
    }

    /**
     * @return string
     */
    public function getWeakness()
    {
        return $this->weakness;
    }

    /**
     * @param string $weakness
     */
    public function setWeakness($weakness)
    {
        $this->weakness = $weakness;
    }

    /**
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param string $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
}