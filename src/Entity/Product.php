<?php

namespace TestJob\Entity;
/**
 * @Entity @Table(name="product")
 **/
class Product
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") **/
    protected $name;
    /** @Column(type="string") **/
    protected $brand;
    /** @Column(type="string") **/
    protected $imgLink;
    /** @Column(type="integer") **/
    protected $offerCount;
    /** @Column(type="integer") **/
    protected $lowPrice;
    /** @Column(type="string") **/
    protected $priceCurrencyCode;
    /** @Column(type="integer") **/
    protected $reviewCount;

    /**
     * @OneToMany(targetEntity="Review", mappedBy="product", cascade={"persist", "remove"})
     * @var Review[] An ArrayCollection of Review objects.
     **/
    protected $reviews = null;


    public function __construct()
    {
        $this->reviews = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getImgLink()
    {
        return $this->imgLink;
    }

    /**
     * @param mixed $imgLink
     */
    public function setImgLink($imgLink)
    {
        $this->imgLink = $imgLink;
    }

    /**
     * @return mixed
     */
    public function getLowPrice()
    {
        return $this->lowPrice;
    }

    /**
     * @param mixed $lowPrice
     */
    public function setLowPrice($lowPrice)
    {
        $this->lowPrice = $lowPrice;
    }

    /**
     * @return mixed
     */
    public function getOfferCount()
    {
        return $this->offerCount;
    }

    /**
     * @param mixed $offerCount
     */
    public function setOfferCount($offerCount)
    {
        $this->offerCount = $offerCount;
    }

    /**
     * @return mixed
     */
    public function getPriceCurrencyCode()
    {
        return $this->priceCurrencyCode;
    }

    /**
     * @param mixed $priceCurrencyCode
     */
    public function setPriceCurrencyCode($priceCurrencyCode)
    {
        $this->priceCurrencyCode = $priceCurrencyCode;
    }

    /**
     * @return mixed
     */
    public function getReviewCount()
    {
        return $this->reviewCount;
    }

    /**
     * @param mixed $reviewCount
     */
    public function setReviewCount($reviewCount)
    {
        $this->reviewCount = $reviewCount;
    }

    public function addReview(Review $review)
    {
        $this->reviews[] = $review;
        $review->setProduct($this);
    }
}