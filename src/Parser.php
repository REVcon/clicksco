<?php

namespace TestJob;


use TestJob\Dto\Product;
use TestJob\Dto\Review;

class Parser
{
    public function parseProducts($content)
    {
        $dom = new \DOMDocument();
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_use_internal_errors($internalErrors);
        $xPath = new \DOMXPath($dom);
        $nodes = $xPath->query("//div[contains(@class, 'cItems_col')]");
        $products = [];
        foreach ($nodes as $node) {
            $product = new Product();
            $product->id = $node->getAttribute("id");

            $tempNode = $xPath->query(".//meta[@itemprop='name']", $node)->item(0);
            if (null == $tempNode){
                continue;
            }
            $product->name = $tempNode->getAttribute("content");

            $tempNode = $xPath->query(".//meta[@itemprop='brand']", $node)->item(0);
            if (null == $tempNode){
                continue;
            }
            $product->brand = $tempNode->getAttribute("content");

            $tempNode = $xPath->query(".//a[contains(@class, 'full-link')]", $node)->item(0);
            if (null == $tempNode){
                continue;
            }
            $product->imgLink = $tempNode->getAttribute("href");

            $tempNode = $xPath->query(".//meta[@itemprop='lowPrice']", $node)->item(0);
            if (null == $tempNode){
                continue;
            }
            $product->lowPrice = $tempNode->getAttribute("content");

            $tempNode = $xPath->query(".//meta[@itemprop='priceCurrency']", $node)->item(0);
            if (null == $tempNode){
                continue;
            }
            $product->priceCurrencyCode = $tempNode->getAttribute("content");

            $tempNode = $xPath->query(".//meta[@itemprop='offerCount']", $node)->item(0);
            if (null == $tempNode){
                continue;
            }
            $product->offerCount = $tempNode->getAttribute("content");

            array_push($products, $product);
        }
        return $products;
    }

    public function parseReviews($content)
    {
        if ($content == "") {
            return [];
        }
        $dom = new \DOMDocument('1.0', 'utf-8');
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_use_internal_errors($internalErrors);
        $xPath = new \DOMXPath($dom);
        $nodes = $xPath->query("//div[contains(@class, 'feedback-i')]");
        $reviews = [];
        foreach ($nodes as $node) {
            $review = new Review();
            $review->title = utf8_decode($xPath->query(".//h3", $node)->item(0)->nodeValue);
            $queryResult = $xPath->query(".//p", $node);
            if (null !== $queryResult->item(0)) {
                $review->strength = utf8_decode($queryResult->item(0)->nodeValue);
            }
            if (null !== $queryResult->item(1)) {
                $review->weakness = utf8_decode($queryResult->item(1)->nodeValue);
            }
            if (null !== $queryResult->item(2)) {
                $review->total = utf8_decode($queryResult->item(2)->nodeValue);
            }
            array_push($reviews, $review);
        }
        return $reviews;
    }

    public function getImageLink($content){
        if ($content == "") {
            return "";
        }
        $dom = new \DOMDocument('1.0', 'utf-8');
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_use_internal_errors($internalErrors);
        $xPath = new \DOMXPath($dom);
        return $xPath->query("//div[contains(@class,'big_image')]/a")->item(0)->getAttribute("href");
    }
}