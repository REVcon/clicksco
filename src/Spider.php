<?php

namespace TestJob;

class Spider
{
    public function query($url)
    {
        $ch = curl_init($url);
        $options = array(
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; W…) Gecko/20100101 Firefox/61.0",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_AUTOREFERER => true,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false
        );
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }
}