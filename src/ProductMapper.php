<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 30.08.2018
 * Time: 20:54
 */

namespace TestJob;


use TestJob\Entity\Product;
use TestJob\Entity\Review;

class ProductMapper
{
    public static function Map($productDto){
        $productEntity = new Product();
        $productEntity->setName($productDto->name);
        $productEntity->setBrand($productDto->brand);
        $productEntity->setImgLink($productDto->imgLink);
        $productEntity->setLowPrice($productDto->lowPrice);
        $productEntity->setPriceCurrencyCode($productDto->priceCurrencyCode);
        $productEntity->setOfferCount($productDto->offerCount);
        $productEntity->setReviewCount($productDto->reviewCount);
        foreach ($productDto->reviews as $review){
            $reviewEntity = new Review();
            $reviewEntity->setTitle($review->title);
            $reviewEntity->setStrength($review->strength);
            $reviewEntity->setWeakness($review->weakness);
            $reviewEntity->setTotal($review->total);
            $productEntity->addReview($reviewEntity);
        }
        return $productEntity;
    }
}