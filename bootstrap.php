<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/Entity"), $isDevMode);

$conn = array(
    'dbname' => 'test_job',
    'user' => 'root',
    'password' => '1111',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
    'charset'  => 'utf8mb4'
);

$entityManager = EntityManager::create($conn, $config);